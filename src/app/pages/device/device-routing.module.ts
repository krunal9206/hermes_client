import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceComponent } from './device.component';
import { InverterComponent } from './inverter/inverter.component';
import { LoggerComponent } from './logger/logger.component';
import { SolarpanelComponent } from './solarpanel/solarpanel.component';

const routes: Routes = [
  {
    path: '',
    component: DeviceComponent,
    children: [
      {
        path: 'inverter',
        component: InverterComponent,
      },
      {
        path: 'logger',
        component: LoggerComponent,
      },
      {
        path: 'solarpanel',
        component: SolarpanelComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeviceRoutingModule {}

export const routedComponents = [DeviceComponent, InverterComponent];
