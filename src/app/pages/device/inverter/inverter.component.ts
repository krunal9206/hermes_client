import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DeviceService } from '../../../_services';

@Component({
  selector: 'ngx-inverter',
  templateUrl: './inverter.component.html',
  styleUrls: ['./inverter.component.scss'],
})
export class InverterComponent implements OnInit {
  deviceInstance: any;
  deviceSummary = {};
  inputsoutputs = {};

  constructor(private router: Router, private service: DeviceService) {}

  ngOnInit() {
    let systemDeviceInstanceId = window.localStorage.getItem(
      'systemDeviceInstanceId',
    );

    if (!systemDeviceInstanceId) {
      this.router.navigate(['pages/grid-dashboard']);
      return;
    }

    this.service.getDataById(+systemDeviceInstanceId).subscribe((data) => {
      this.deviceInstance = data;
    });

    this.service.getDeviceData(+systemDeviceInstanceId).subscribe((data) => {
      this.deviceSummary = data;
    });

    this.service.getInputsOutputs(+systemDeviceInstanceId).subscribe((data) => {
      this.inputsoutputs = data;
    });
  }
}
