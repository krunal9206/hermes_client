import { NgModule } from '@angular/core';
import {
  NbCardModule,
  NbListModule,
  NbIconModule,
  NbInputModule,
  NbTreeGridModule,
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { DeviceRoutingModule, routedComponents } from './device-routing.module';
import { LoggerComponent } from './logger/logger.component';
import { SolarpanelComponent } from './solarpanel/solarpanel.component';

@NgModule({
  imports: [
    NbCardModule,
    NbTreeGridModule,
    NbListModule,
    NbIconModule,
    NbInputModule,
    ThemeModule,
    DeviceRoutingModule,
  ],
  declarations: [...routedComponents, LoggerComponent, SolarpanelComponent],
})
export class DeviceModule {}
