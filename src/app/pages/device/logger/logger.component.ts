import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DeviceService } from '../../../_services';

@Component({
  selector: 'ngx-logger',
  templateUrl: './logger.component.html',
  styleUrls: ['./logger.component.scss'],
})
export class LoggerComponent implements OnInit {
  deviceInstance: any;
  communicationMode = '';
  connectedDevice = {};

  constructor(private router: Router, private service: DeviceService) {}

  ngOnInit(): void {
    let systemDeviceInstanceId = window.localStorage.getItem(
      'systemDeviceInstanceId',
    );

    if (!systemDeviceInstanceId) {
      this.router.navigate(['pages/grid-dashboard']);
      return;
    }

    this.service.getDataById(+systemDeviceInstanceId).subscribe((data) => {
      this.deviceInstance = data;

      try {
        const addlAttributesSetupByAdmin = JSON.parse(
          this.deviceInstance.addlAttributesSetupByAdmin,
        );

        if (addlAttributesSetupByAdmin) {
          const sectionDetails = addlAttributesSetupByAdmin[0].sectionDetails;

          let communicationMode = sectionDetails.find(
            (o) => o.key === `communicationMode`,
          );

          if (communicationMode) {
            this.communicationMode = communicationMode.value;
          }
        }
      } catch (error) {}

      console.log(this.deviceInstance);
    });

    this.service
      .getLoggerConnectedDevice(+systemDeviceInstanceId)
      .subscribe((data) => {
        this.connectedDevice = data;
      });
  }
}
