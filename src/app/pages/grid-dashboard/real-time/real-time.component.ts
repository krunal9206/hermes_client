import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthenticationService, DashboardService } from '../../../_services';
import { LocalDataSource, ServerDataSource } from 'ng2-smart-table';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-real-time',
  styleUrls: ['./real-time.component.scss'],
  templateUrl: './real-time.component.html',
})
export class RealTimeComponent implements OnInit {
  plantInfoSections: any[];
  serverSource: ServerDataSource;
  stakeholder: any;
  deviceTypes = [];
  devices = [];

  settings = {
    actions: false,
    pager: {
      perPage: 20,
    },
    columns: {
      systemErrWrnTxnId: {
        title: 'ID',
        type: 'number',
        filter: false,
      },
      serialNo: {
        title: 'Sr. No.',
        type: 'string',
        filter: false,
      },
      givenDeviceErrWrnCode: {
        title: 'Error Code',
        type: 'string',
        filter: false,
      },
      errWrnCodeType: {
        title: 'Type',
        type: 'string',
        filter: false,
      },
      errWrnDescription: {
        title: 'Detail',
        type: 'string',
        filter: false,
      },
      generatedDateTime: {
        title: 'Time',
        type: 'string',
        filter: false,
      },
    },
  };

  loggerSettings = {
    actions: false,
    pager: {
      perPage: 20,
    },
    columns: {
      serialNo: {
        title: 'Sr. No.',
        type: 'string',
        filter: false,
      },
      brand: {
        title: 'Brand',
        type: 'string',
        filter: false,
        defaultValue: 'Hermes',
      },
      lastUpdate: {
        title: 'Last Update',
        type: 'string',
        filter: false,
        defaultValue: new Date().toLocaleDateString('en-US'),
      },
    },
  };

  inverterSettings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
      custom: [
        {
          name: 'view',
          title: 'View',
        },
      ],
    },
    pager: {
      perPage: 20,
    },
    columns: {
      serialNo: {
        title: 'Sr. No.',
        type: 'string',
        filter: false,
      },
      parentLogger: {
        title: 'Logger',
        type: 'string',
        filter: false,
        defaultValue: 'Hermes',
      },
      lastUpdate: {
        title: 'Last Update',
        type: 'string',
        filter: false,
        defaultValue: new Date().toLocaleDateString('en-US'),
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  loggerSource: LocalDataSource = new LocalDataSource();
  inverterSource: LocalDataSource = new LocalDataSource();

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private service: DashboardService,
    public router: Router,
  ) {}

  ngOnInit() {
    const user: any = this.authenticationService.currentUserValue;
    const assestIds = user.assestIds;

    this.service.getPlantInfo({ assestIds }).subscribe((data) => {
      if (data[0].cardTypeAttr) {
        this.stakeholder = data[0].stakeholder;
        const attributes = [];

        let plantInfoSections = JSON.parse(data[0].deviceTypeAttr);

        let dbAddAttributes = JSON.parse(data[0].deviceAttr);

        let i = 0;
        for (const section of plantInfoSections.sections) {
          const sectionField = [];
          let j = 0;
          for (const key in section.sectionDetails) {
            if (section.sectionDetails.hasOwnProperty(key)) {
              let dBvalue = '';
              if (dbAddAttributes) {
                try {
                  if (dbAddAttributes[i]['sectionDetails'][j]['value']) {
                    dBvalue = dbAddAttributes[i]['sectionDetails'][j]['value'];
                  }
                } catch (e) {}
              }

              const title = section.sectionDetails[key];

              sectionField.push({
                key,
                value: dBvalue,
                title,
              });

              j++;
            }
          }

          attributes.push({
            sectionName: section.sectionName,
            sectionDetails: sectionField,
          });
          i++;
        }

        plantInfoSections = JSON.parse(data[0].cardTypeAttr);

        dbAddAttributes = JSON.parse(data[0].cardAttr);

        i = 0;
        for (const section of plantInfoSections.sections) {
          const sectionField = [];
          let j = 0;
          for (const key in section.sectionDetails) {
            if (section.sectionDetails.hasOwnProperty(key)) {
              let dBvalue = '';
              if (dbAddAttributes) {
                try {
                  if (dbAddAttributes[i]['sectionDetails'][j]['value']) {
                    dBvalue = dbAddAttributes[i]['sectionDetails'][j]['value'];
                  }
                } catch (e) {}
              }

              const title = section.sectionDetails[key];

              sectionField.push({
                key,
                value: dBvalue,
                title,
              });

              j++;
            }
          }

          attributes.push({
            sectionName: section.sectionName,
            sectionDetails: sectionField,
          });
          i++;
        }

        this.plantInfoSections = attributes;
      } else {
        this.plantInfoSections = [];
      }
    });

    this.serverSource = new ServerDataSource(this.http, {
      endPoint: `${environment.apiUrl}/dashboard/alerts`,
      dataKey: 'data',
      totalKey: 'total',
      perPage: 'per_page',
      pagerPageKey: 'page',
    });

    this.service.getDeviceInfo({ assestIds }).subscribe((data) => {
      console.log(data);
      // this.loggerSource.load(data.loggers);
      // this.inverterSource.load(data.devices);
      this.deviceTypes = data.devicetypes;
      this.devices = data.devices;
    });
  }

  onCustom(event: any) {
    window.localStorage.removeItem('systemDeviceInstanceId');
    window.localStorage.setItem(
      'systemDeviceInstanceId',
      event.data.systemDeviceInstanceId.toString(),
    );
    this.router.navigate(['pages/device/inverter']);
  }

  devicePage(systemDeviceInstanceId, deviceTypeName) {
    window.localStorage.removeItem('systemDeviceInstanceId');
    window.localStorage.setItem(
      'systemDeviceInstanceId',
      systemDeviceInstanceId.toString(),
    );

    if (deviceTypeName.toLowerCase() == 'inverter') {
      this.router.navigate(['pages/device/inverter']);
    } else if (deviceTypeName.toLowerCase() == 'logger') {
      this.router.navigate(['pages/device/logger']);
    } else if (deviceTypeName.toLowerCase() == 'solar_panel') {
      this.router.navigate(['pages/device/solarpanel']);
    }
  }

  unsorted() {}
}
