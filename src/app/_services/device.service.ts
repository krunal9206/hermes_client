import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DeviceService {
  constructor(private http: HttpClient) {}

  getDataById(id: number) {
    return this.http.get<any>(`${environment.apiUrl}/front/device/info/${id}`);
  }

  getDeviceData(id: number) {
    return this.http.get<any>(
      `${environment.apiUrl}/front/device/summery/${id}`,
    );
  }

  getInputsOutputs(id: number) {
    return this.http.get<any>(`${environment.apiUrl}/front/device/ios/${id}`);
  }

  getLoggerConnectedDevice(id: number) {
    return this.http.get<any>(
      `${environment.apiUrl}/front/device/loggerconnecteddevice/${id}`,
    );
  }
}
