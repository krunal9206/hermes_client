import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

interface InterfaceCard {
  id: number;
  cardTypeAttr: any;
}

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  getPlantInfo(assestIds: any) {
    return this.http.post<InterfaceCard>(
      `${environment.apiUrl}/dashboard/plantinfo`,
      assestIds,
    );
  }

  getDeviceInfo(assestIds: any) {
    return this.http.post<any>(
      `${environment.apiUrl}/dashboard/deviceinfo`,
      assestIds,
    );
  }
}
