export class User {
  id: number;
  givenUserId: string;
  stakeholderId: number;
  username: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  status: string;
  token?: string;
  userrole: [];
}
